<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Admin1',
        	'email' => 'afhamalias65@gmail.com',
        	'password' => '$2y$10$mVz86JReKMuf6piwk5.W.OmARSM8hVQe5PRO7B54ONgL1FlV.B62G',
        ]);

        DB::table('folders')->insert([
        	'title' => 'Majlis 1',
        	'date' => '2021-12-22',
        ]);

        DB::table('folders')->insert([
        	'title' => 'Majlis 2',
        	'date' => '2021-12-15',
        ]);

        DB::table('folders')->insert([
        	'title' => 'Majlis 3',
        	'date' => '2021-12-15',
        ]);

        DB::table('folders')->insert([
        	'title' => 'Majlis 4',
        	'date' => '2021-12-15',
        ]);

        DB::table('folders')->insert([
        	'title' => 'Majlis 5',
        	'date' => '2021-12-15',
        ]);

        DB::table('folders')->insert([
        	'title' => 'Majlis 6',
        	'date' => '2021-12-15',
        ]);

        DB::table('folders')->insert([
        	'title' => 'Majlis 7',
        	'date' => '2021-12-15',
        ]);
    }

}
