<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

include 'multimedia/music.php';
include 'multimedia/picture.php';
include 'multimedia/streaming.php';
include 'multimedia/video.php';


// Picture admin Upload
Route::get('/uploadPicture', [App\Http\Controllers\UploadController::class, 'uploadPicture']);
Route::post('/upload/proses/picture', 'UploadController@proses_upload');
Route::get('/uploadPicture/delete{id}', [App\Http\Controllers\UploadController::class, 'deletePicture']);
Route::post('/upload/proses/picture', [App\Http\Controllers\UploadController::class, 'proses_uploadPicture'])->name('upload.proses');

// Create folder
Route::get('/folder', [App\Http\Controllers\folderController::class, 'folder']);
Route::post('/store', [App\Http\Controllers\folderController::class, 'store'])->name('store');
Route::get('/delete{id}', [App\Http\Controllers\folderController::class, 'deleteFolder']);



// Audio admin upload
Route::get('/uploadAudio', [App\Http\Controllers\adminAudioController::class, 'uploadAudio']);
// Route::post('/upload/proses', 'adminAudioController@proses_upload');
Route::post('/upload/proses/audio', [App\Http\Controllers\adminAudioController::class, 'proses_uploadAudio'])->name('upload.proses');
Route::get('/uploadAudio/delete{id}', [App\Http\Controllers\adminAudioController::class, 'deleteAudio']);




// Video
Route::get('/uploadVideo', [App\Http\Controllers\adminVideoController::class,'uploadVideo']);
Route::post('/upload/proses/video', [App\Http\Controllers\adminVideoController::class,'proses_uploadVideo'])->name('upload.proses');
Route::get('/uploadVideo/delete{id}', [App\Http\Controllers\adminVideoController::class,'deleteVideo']);




// Streaming
Route::get('/editStream', [App\Http\Controllers\adminStreamController::class,'editStream']);
Route::post('/updateData', [App\Http\Controllers\adminStreamController::class,'updateData']);
