<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\StreamingController::class, 'index'])->name('streaming.index');

Route::get('admin/editStream', [App\Http\Controllers\StreamingController::class, 'streamEdit'])->name('admin.editStream');
