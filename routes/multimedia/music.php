<?php

use Illuminate\Support\Facades\Route;


Route::get('music.index', [App\Http\Controllers\MusicController::class, 'index'])->name('music.index');

Route::get('music.details/{id}', [App\Http\Controllers\MusicController::class, 'details'])->name('music.details');



// Route::get('admin/editAudio', [App\Http\Controllers\MusicController::class, 'audioEdit'])->name('admin.editAudio');
