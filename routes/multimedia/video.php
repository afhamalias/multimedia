<?php

use Illuminate\Support\Facades\Route;

Route::get('video.gallery', [App\Http\Controllers\VideoController::class, 'index'])->name('video.gallery');


Route::get('video.player/{id}', [App\Http\Controllers\VideoController::class, 'videoPlayer'])->name('video.player');


// Route::get('admin/videoStream', [App\Http\Controllers\VideoController::class, 'videoEdit'])->name('admin.editVideo');

// Route::get('/video.player', function () {
//     return view('video.player');
// });

