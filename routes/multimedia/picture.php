<?php

use Illuminate\Support\Facades\Route;

Route::get('picture.index', [App\Http\Controllers\PictureController::class, 'index'])->name('picture.index');

Route::get('picture.page2/{id}', [App\Http\Controllers\PictureController::class, 'pictureView'])->name('picture.View');

// Route::get('admin/editPicture', [App\Http\Controllers\PictureController::class, 'pictureEdit'])->name('admin.editPicture');
