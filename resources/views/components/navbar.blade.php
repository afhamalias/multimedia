<style>
    @import url('https://fonts.googleapis.com/css2?family=Lobster&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Zen+Antique+Soft&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Carter+One&family=Kaushan+Script&family=Lemon&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Paprika&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Rubik:wght@300&display=swap');
/*
    .title {
        font-family: 'Rubik', sans-serif;
        font-size: 30px;
        color: black;
        display: inline-block;
        -webkit-transform:scale(2,1);
        text-align: center
        padding-left: 65px;
    } */

    .bg-img {
        /* background-image: url("banner.jpg"); */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;

        /* Needed to position the navbar */
        position: relative;
    }

    .nav-item
    {
        color: black;
    }

    .icon
    {
        font-size: 18px;
        padding-left: 5px;
        color: black;
    }

</style>




<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white bg-img">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button" style="color: maroon">
                <i class="fas fa-bars" style="margin-top: 15%">
                </i>
            </a>
        </li>


        <li class="nav-item d-none d-sm-inline-block">

            <div class="mb-20">
                <span class="title" style="font-size: 20px; color: grey; letter-spacing: 4px; ">
                    <b> PORTAL SUMBER </b>
                </span>

                <br>

                <span style="font-size: 30px; letter-spacing: 7px;">
                    <b> MULTIMEDIA</b>
                </span>

                <br>

                <span class="title" style="font-size: 20px; color:grey; letter-spacing: 3px; "> <b> NEGERI <span>SELANGOR</b></span></span>
            </div>
        </li>

    </ul>





    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">




        {{-- <li class="nav-item">
            <a class="nav-link"  href="{{ route('login') }}" role="button">
                <i class="fas fa-sign-in-alt"></i>
                <span class="icon"> Login </span>
            </a>
        </li> --}}




    </ul>
</nav>

