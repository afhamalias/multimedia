<style>
    .mt {
        margin-top: 10%;
    }

    .active {
        background-color: #b90f0f !important;
    }

    .ic {
        color: white;
    }



</style>





<ul class="nav nav-pills nav-sidebar flex-column mt" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
   with font-awesome or any other icon font library -->




    <li class="nav-item ">
        <a href="{{ route('streaming.index') }}" class="nav-link">
            <i class="nav-icon fas fa-home ic"></i>
            <p style="font-size: 23px">
                Halaman Utama
            </p>
        </a>
    </li>



    <li class="nav-item">
        <a href="{{ route('video.gallery') }}" class="nav-link ">
            <i class="nav-icon fas fa-video ic"></i>

            <p style="font-size: 23px">
                Galeri Video
            </p>
        </a>
    </li>



    <li class="nav-item">
        <a href="{{ route('picture.index') }}" class="nav-link">
            <i class="nav-icon fas fa-images ic"></i>
            <p style="font-size: 23px">
                Galeri Gambar
            </p>
        </a>
    </li>



    <li class="nav-item">
        <a href="{{ route('music.index') }}" class="nav-link">
            <i class="nav-icon fas fa-headphones ic"></i>
            <p style="font-size: 23px">
                Galeri Audio
            </p>
        </a>
    </li>





    {{-- <li class="nav-item menu-open">


        <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
                Starter Pages
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="#" class="nav-link active">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Active Page</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Inactive Page</p>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
                Simple Link
                <span class="right badge badge-danger">New</span>
            </p>
        </a>
    </li> --}}


</ul>
