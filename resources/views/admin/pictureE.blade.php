@extends('layouts.app')

@section('content')

<style>
    .line-1 {
        height: 1px;
        background: black;
    }

</style>



<div class="row">
		<div class="container">

			<h2 class="text-center my-5"> Upload or Delete Picture </h2>

			<div class="col-lg-8 mx-auto my-5">

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('delete'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif


            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }} <br />
                    @endforeach
                </div>
            @endif


				@if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif

				<form action="/upload/proses/picture" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group">
						<b>Picture</b><br/>
						<input type="file" name="picture"> <br>
                        <b style="color: red">Image of size 1920 x 1080 pixels is required </b>
					</div>

                    <div class="form-group">
						<b>Title</b>
						<textarea class="form-control" name="title" placeholder="Insert title for this image"></textarea>
					</div>

					<div class="form-group">
						<b>Description</b>
						<textarea class="form-control" name="description" placeholder="Insert description for this image"></textarea>
					</div>

                    <div class="form-group">
                        <b>Date</b><br />
                        <input type="date" name="date">
                    </div>

                    <div class="form-group">
						<b>Folder Name</b>
                        <select class="form-control" name="folder_id">

                            @foreach ($folder as $f)
                                <option value="{{$f->id}}">{{$f->title}}</option>
                            @endforeach
                        </select>
					</div>

					<input type="submit" value="Upload" class="btn btn-primary">
                    <a href="home" class="btn btn-primary"> Back</a>
				</form>

                <br>

                <div class="line-1"></div>

				<h4 class="my-5">Data</h4>

				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="1%">Picture</th>
                            <th>Title</th>
							<th>Decription</th>
                            <th>Date</th>
                            <th>Folder ID</th>
							<th width="1%">Option</th>
						</tr>
					</thead>
					<tbody>
						@foreach($gambar as $g)
						<tr>
							<td><img width="150px" src="{{ url('/data_file/'.$g->picture) }}"></td>
							<td>{{$g->title}}</td>
                            <td>{{$g->description}}</td>
                            <td>{{$g->date}}</td>
                            <td>{{$g->folder_id}}</td>
							<td><a class="btn btn-danger" href="/uploadPicture/delete{{ $g->id }}">HAPUS</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
