@extends('layouts.app')

@section('content')


    <style>

        img {
            width: 450px;
            height: 250px;
        }


        .line-1 {
            height: 1px;
            background: black;
        }

    </style>

    <div class="row">
        <div class="container">
            <h2 class="text-center my-5">Upload or Delete Video</h2>

            <div class="col-lg-8 mx-auto my-5">

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('delete'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif


            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }} <br />
                    @endforeach
                </div>
            @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br />
                        @endforeach
                    </div>
                @endif



                <form action="/upload/proses/video" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <b>File Video</b><br>
                        <input type="file" name="video">
                    </div>

                    <div class="form-group">
                        <b>Thumbnail Untuk Video</b><br />
                        <input type="file" name="thumbnail"><br>
                        <b style="color: red">Image of size 1920 x 1080 pixels is required</b>
                    </div>

                    <br>

                    <div class="form-group">
                        <b>Title</b>
                        <textarea class="form-control" name="title"
                            placeholder="Please insert title for this video"></textarea>
                    </div>

                    <div class="form-group">
                        <b>Description</b>
                        <textarea class="form-control" name="description"
                            placeholder="Please insert Description fot this video"></textarea>
                    </div>

                    <div class="form-group">
                        <b>Date</b><br />
                        <input type="date" name="date">
                    </div>


                    <input type="submit" value="Upload" class="btn btn-primary">
                    <a href="home" class="btn btn-primary"> Back</a>
                    <br><br>
                </form>

                <br>

                <div class="line-1"></div>

                <h4 class="my-5">Data</h4>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>

                            <th width="1%">Thumbnail Video</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Upload Date</th>
                            <th width="1%">Option</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($video as $v)
                            <tr>
                                <td>
                                    <img src="{{ url('/data_file/' . $v->thumbnail) }}" alt="">
                                </td>


                                <td>{{ $v->title }}</td>


                                <td>{{ $v->description }}</td>

                                <td>{{ $v->date }}</td>
                                <td><a class="btn btn-danger" href="/uploadVideo/delete{{ $v->id }}">HAPUS</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
