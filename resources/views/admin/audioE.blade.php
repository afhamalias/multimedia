@extends('layouts.app')

@section('content')

    <style>
        .line-1 {
            height: 1px;
            background: black;
        }

    </style>




    <div class="row">
        <div class="container">
            <h2 class="text-center my-5">Upload or Delete Audio</h2>

            <div class="col-lg-8 mx-auto my-5">


                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                @if ($message = Session::get('delete'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br />
                        @endforeach
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br />
                        @endforeach
                    </div>
                @endif

                <form action="/upload/proses/audio" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <b>File Audio</b><br />
                        <input type="file" name="audio">
                    </div>

                    <div class="form-group">
                        <b>Title</b>
                        <textarea class="form-control" name="title"
                            placeholder="Please insert title for this audio"></textarea>
                    </div>

                    <div class="form-group">
                        <b>Description</b>
                        <textarea class="form-control" name="description"
                            placeholder="Please insert Description fot this audio"></textarea>
                    </div>

                    <div class="form-group">
                        <b>Date</b><br />
                        <input type="date" name="date">
                    </div>


                    <input type="submit" value="Upload" class="btn btn-primary">

                    <a href="home" class="btn btn-primary"> Back</a>

                    <br><br>
                </form>

                <div class="line-1"></div>

                <h4 class="my-5">Data</h4>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="1%">File Audio</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Upload Date</th>
                            <th width="1%">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($audio as $m)
                            <tr>
                                <td>
                                    <audio controls>
                                        <source src="{{ url('/data_file/' . $m->audio) }}" type="audio/mpeg">
                                    </audio>
                                </td>

                                <td>{{ $m->title }}</td>

                                <td>{{ $m->description }}</td>

                                <td>{{ $m->date }}</td>
                                <td><a class="btn btn-danger" href="/uploadAudio/delete{{ $m->id }}">HAPUS</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
