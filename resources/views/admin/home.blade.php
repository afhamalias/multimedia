@extends('layouts.app')





@section('content')

<style>

    .btn
    {
        border-color: black;
        color: white;

    }

    /* .warna1
    {
        border-color: maroon;
        border-width: 4px;
    }

    .warna2
    {
        border-color: green;

    }

    .warna3
    {
        border-color: yellow;

    }

    .warna4
    {
        border-color: blue;
        border-width: 4px;
    } */



    b{
        font-size: 20px;
    }

</style>




    <div class="container">
        <div class="row justify-content-center">


            <div class="card col-lg-6">
                <div class="card-header">
                    <b> Streaming Page</b>
                </div>
                <div class="card-body">

                    <a href="{{ route('admin.editStream') }}" class="btn btn-primary" style="pointer-events: none;">Edit Content</a>
                </div>
            </div>





            <div class="card col-lg-6">
                <div class="card-header">
                    <b> Video Gallery Page</b>
                </div>
                <div class="card-body">

                    <a href="{{ url('/uploadVideo') }}" class="btn btn-primary">Edit Content</a>
                </div>
            </div>





            <div class="card  col-lg-6">
                <div class="card-header">
                    <b> Photo Gallery Page</b>
                </div>
                <div class="card-body">

                    <a href="{{ url('/folder') }}" class="btn btn-primary">Create Folder</a>
                    <a href="{{ url('/uploadPicture') }}" class="btn btn-primary">Edit Content</a>

                </div>
            </div>



            <div class="card col-lg-6">
                <div class="card-header">
                    <b> Audio Gallery Page</b>
                </div>
                <div class="card-body">

                    <a href="{{ url('/uploadAudio') }}" class="btn btn-primary">Edit Content</a>
                </div>
            </div>




            {{-- <div class="col">
                <div class="card">
                    <div class="card-header">{{ __('Audio Gallery') }}</div>

                    <div class="card-body">

                        <a href="admin.streamE">
                            <input type="submit" value="Edit" />
                        </a </div>
                    </div>
                </div>
            </div> --}}



        </div>
    </div>
@endsection
