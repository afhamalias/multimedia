@extends('layouts.app')

@section('content')

    <style>
        .line-1 {
            height: 1px;
            background: black;
        }

    </style>



    <div class="row">
        <div class="container">

            <h2 class="text-center my-5"> Create Folder </h2>

            <div class="col-lg-8 mx-auto my-5">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                @if ($message = Session::get('delete'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br />
                        @endforeach
                    </div>
                @endif

                <form action="/store" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}


                    <div class="form-group">
                        <b>Title</b>
                        <textarea class="form-control" name="title" placeholder="Please insert title for the folder"></textarea>
                    </div>

                    <div class="form-group">
                        <b>Date</b><br />
                        <input type="date" name="date">
                    </div>

                    <input type="submit" value="Upload" class="btn btn-primary">
                    <a href="home" class="btn btn-primary"> Back</a>
                </form>

                <br>

                <div class="line-1"></div>

                <h4 class="my-5">Data</h4>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Folder ID</th>
                            <th>Folder Name</th>
                            <th>Created Date</th>
                            <th width="1%">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($folder as $f)
                            <tr>
                                <td>{{ $f->id }}</td>
                                <td>{{ $f->title }}</td>
                                <td>{{ $f->date }}</td>

                                <td><a class="btn btn-danger" href="/delete{{ $f->id }}">DELETE</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@prepend('script')




@endprepend
