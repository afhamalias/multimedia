@extends('layouts.master')

@prepend('style')

    <style>
        .title {
            font-size: 20px;
            color: grey;
        }



        .frame {
            width: 100%;
            height: 600px;
        }

        .moreVid {
            width: 100%;
            height: 200px;

        }

        h1 {
            font-size: 30px;
            text-align: center;
            /* font-family: 'Zen Kurenaido', sans-serif; */
        }

        img.icon {
            width: 60px;
            height: 70px;
            padding-bottom: 10px;
        }

        p.des {
            font-size: 30px;
            text-align: center;


        }

        .bg {
            background-color: transparent;
            color: black;
        }

        .margin1 {
            padding-block-start: 10px;
        }

        .margin2 {
            padding-block-start: 10px;
        }




        /* Youtube Card Link */
        @import url('https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

        .containerr {
            position: relative;
        }

        .containerr .card {
            position: relative;
            width: 320px;
            height: 450px;
            background: #232323;
            border-radius: 20px;
            overflow: hidden;
        }

        .containerr .card:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: white;
            clip-path: circle(150px at 80% 20%);
            transition: 0.5s ease-in-out;
        }

        .containerr .card:hover:before {
            clip-path: circle(300px at 80% -20%);
        }

        .containerr .card:after {
            content: 'Youtube';
            position: absolute;
            top: 30%;
            left: -20%;
            font-size: 12em;
            font-weight: 800;
            font-style: italic;
            color: rgba(255, 255, 25, 0.05)
        }

        .containerr .card .imgBx {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            z-index: 10000;
            width: 100%;
            height: 220px;
            transition: 0.5s;
        }

        .containerr .card:hover .imgBx {
            top: 0%;
            transform: translateY(0%);

        }

        .containerr .card .imgBx img {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 200px;
        }

        .containerr .card .contentBx {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 100px;
            text-align: center;
            transition: 1s;
            z-index: 10;
        }

        .containerr .card:hover .contentBx {
            height: 210px;
        }

        .containerr .card .contentBx h2 {
            position: relative;
            font-weight: 600;
            letter-spacing: 1px;
            color: #fff;
            margin: 0;
        }

        .containerr .card .contentBx .size,
        .containerr .card .contentBx .color {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 8px 20px;
            transition: 0.5s;
            opacity: 0;
            visibility: hidden;
            padding-top: 0;
            padding-bottom: 0;
        }

        .containerr .card:hover .contentBx .size {
            opacity: 1;
            visibility: visible;
            transition-delay: 0.5s;
        }

        .containerr .card:hover .contentBx .color {
            opacity: 1;
            visibility: visible;
            transition-delay: 0.6s;
        }

        .containerr .card .contentBx .size h3,
        .containerr .card .contentBx .color h3 {
            color: #fff;
            font-weight: 300;
            font-size: 14px;
            text-transform: uppercase;
            letter-spacing: 2px;
            margin-right: 10px;
        }

        .containerr .card .contentBx .size span {
            width: 26px;
            height: 26px;
            text-align: center;
            line-height: 26px;
            font-size: 14px;
            display: inline-block;
            color: #111;
            background: #fff;
            margin: 0 5px;
            transition: 0.5s;
            color: #111;
            border-radius: 4px;
            cursor: pointer;
        }



        .containerr .card .contentBx a {
            display: inline-block;
            padding: 10px 20px;
            background: #fff;
            border-radius: 4px;
            margin-top: 10px;
            text-decoration: none;
            font-weight: 600;
            color: #111;
            opacity: 0;
            transform: translateY(50px);
            transition: 0.5s;
            margin-top: 0;
        }

        .containerr .card:hover .contentBx a {
            opacity: 1;
            transform: translateY(0px);
            transition-delay: 0.75s;

        }

        /* end */



        /* Facebook Card Link */
        .containerr2 {
            position: relative;
        }

        .containerr2 .card2 {
            position: relative;
            width: 320px;
            height: 450px;
            background: #232323;
            border-radius: 20px;
            overflow: hidden;
        }

        .containerr2 .card2:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: white;
            clip-path: circle(150px at 80% 20%);
            transition: 0.5s ease-in-out;
        }

        .containerr2 .card2:hover:before {
            clip-path: circle(300px at 80% -20%);
        }

        .containerr2 .card2:after {
            content: 'Facebook';
            position: absolute;
            top: 30%;
            left: -20%;
            font-size: 12em;
            font-weight: 800;
            font-style: italic;
            color: rgba(255, 255, 25, 0.05)
        }

        .containerr2 .card2 .imgBx2 {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            z-index: 10000;
            width: 100%;
            height: 220px;
            transition: 0.5s;
        }

        .containerr2 .card2:hover .imgBx2 {
            top: 0%;
            transform: translateY(0%);

        }

        .containerr2 .card2 .imgBx2 img {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 200px;
        }

        .containerr2 .card2 .contentBx2 {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 100px;
            text-align: center;
            transition: 1s;
            z-index: 10;
        }

        .containerr2 .card2:hover .contentBx2 {
            height: 210px;
        }

        .containerr2 .card2 .contentBx2 h2 {
            position: relative;
            font-weight: 600;
            letter-spacing: 1px;
            color: #fff;
            margin: 0;
        }

        .containerr2 .card2 .contentBx2 .size2,
        .containerr2 .card2 .contentBx2 .color2 {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 8px 20px;
            transition: 0.5s;
            opacity: 0;
            visibility: hidden;
            padding-top: 0;
            padding-bottom: 0;
        }

        .containerr2 .card2:hover .contentBx2 .size2 {
            opacity: 1;
            visibility: visible;
            transition-delay: 0.5s;
        }

        .containerr2 .card2:hover .contentBx2 .color2 {
            opacity: 1;
            visibility: visible;
            transition-delay: 0.6s;
        }

        .containerr2 .card2 .contentBx2 .size2 h3,
        .containerr2 .card2 .contentBx2 .color2 h3 {
            color: #fff;
            font-weight: 300;
            font-size: 14px;
            text-transform: uppercase;
            letter-spacing: 2px;
            margin-right: 10px;
        }

        .containerr2 .card2 .contentBx2 .size2 span {
            width: 26px;
            height: 26px;
            text-align: center;
            line-height: 26px;
            font-size: 14px;
            display: inline-block;
            color: #111;
            background: #fff;
            margin: 0 5px;
            transition: 0.5s;
            color: #111;
            border-radius: 4px;
            cursor: pointer;
        }



        .containerr2 .card2 .contentBx2 a {
            display: inline-block;
            padding: 10px 20px;
            background: #fff;
            border-radius: 4px;
            margin-top: 10px;
            text-decoration: none;
            font-weight: 600;
            color: #111;
            opacity: 0;
            transform: translateY(50px);
            transition: 0.5s;
            margin-top: 0;
        }

        .containerr2 .card2:hover .contentBx2 a {
            opacity: 1;
            transform: translateY(0px);
            transition-delay: 0.75s;

        }


        /* end */




        /* Tiktok Card Link */

        .containerr3 {
            position: relative;
        }

        .containerr3 .card3 {
            position: relative;
            width: 320px;
            height: 450px;
            background: #232323;
            border-radius: 20px;
            overflow: hidden;
        }

        .containerr3 .card3:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: white;
            clip-path: circle(150px at 80% 20%);
            transition: 0.5s ease-in-out;
        }

        .containerr3 .card3:hover:before {
            clip-path: circle(300px at 80% -20%);
        }

        .containerr3 .card3:after {
            content: 'tiktok';
            position: absolute;
            top: 30%;
            left: -20%;
            font-size: 12em;
            font-weight: 800;
            font-style: italic;
            color: rgba(255, 255, 25, 0.05)
        }

        .containerr3 .card3 .imgBx3 {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            z-index: 10000;
            width: 100%;
            height: 220px;
            transition: 0.5s;
        }

        .containerr3 .card3:hover .imgBx3 {
            top: 0%;
            transform: translateY(0%);

        }

        .containerr3 .card3 .imgBx3 img {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 200px;
        }

        .containerr3 .card3 .contentBx3 {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 100px;
            text-align: center;
            transition: 1s;
            z-index: 10;
        }

        .containerr3 .card3:hover .contentBx3 {
            height: 210px;
        }

        .containerr3 .card3 .contentBx3 h2 {
            position: relative;
            font-weight: 600;
            letter-spacing: 1px;
            color: #fff;
            margin: 0;
        }

        .containerr3 .card3 .contentBx3 .size3,
        .containerr3 .card3 .contentBx3 .color3 {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 8px 20px;
            transition: 0.5s;
            opacity: 0;
            visibility: hidden;
            padding-top: 0;
            padding-bottom: 0;
        }

        .containerr3 .card3:hover .contentBx3 .size3 {
            opacity: 1;
            visibility: visible;
            transition-delay: 0.5s;
        }

        .containerr3 .card3:hover .contentBx3 .color3 {
            opacity: 1;
            visibility: visible;
            transition-delay: 0.6s;
        }

        .containerr3 .card3 .contentBx3 .size3 h3,
        .containerr3 .card3 .contentBx3 .color3 h3 {
            color: #fff;
            font-weight: 300;
            font-size: 14px;
            text-transform: uppercase;
            letter-spacing: 2px;
            margin-right: 10px;
        }

        .containerr3 .card3 .contentBx3 .size3 span {
            width: 26px;
            height: 26px;
            text-align: center;
            line-height: 26px;
            font-size: 14px;
            display: inline-block;
            color: #111;
            background: #fff;
            margin: 0 5px;
            transition: 0.5s;
            color: #111;
            border-radius: 4px;
            cursor: pointer;
        }



        .containerr3 .card3 .contentBx3 a {
            display: inline-block;
            padding: 10px 20px;
            background: #fff;
            border-radius: 4px;
            margin-top: 10px;
            text-decoration: none;
            font-weight: 600;
            color: #111;
            opacity: 0;
            transform: translateY(50px);
            transition: 0.5s;
            margin-top: 0;
        }

        .containerr3 .card3:hover .contentBx3 a {
            opacity: 1;
            transform: translateY(0px);
            transition-delay: 0.75s;

        }


        /* end */

    </style>

@endprepend



@section('content')


    <div class="container">


        <div class="row justify-content-center">




            <div class="col-lg-12">
                <b style="letter-spacing: 3px; ">
                    <p class="des"> <b> Siaran Lansung dari Dewan Selangor </b> </p>

                </b>
            </div>


            {{-- Video live --}}
            <div class="col">

                <div  id="playerElement" style="width:100%; height:0; padding:0 0 56.25% 0"></div>



                {{-- <center>
                    <iframe src="https://www.youtube.com/embed/OyQQOrbLflE" frameborder="0" allowfullscreen
                        ng-show="showvideo">
                    </iframe>
                </center> --}}

                <br>



                {{-- Tajuk besar --}}
                <b style="letter-spacing: 5px; ">
                    <h1> SIARAN <img class="icon" src="live-streaming-icon.png"> LANGSUNG </h1>
                </b>



                <br>


                {{-- Description Below / Live Description --}}
                <div class="card card-primary border border-danger">
                    <div class="card-body">

                        {{-- Tarikh --}}
                        <h5 class="card-title" style="font-size: 20px"> <b> Tarikh : </b></h5>

                        <br><br>


                        <!-- Description -->
                        <div class="panel-group">
                            {{-- Description / Keterangan --}}
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">


                                        <p style="font-size: 20px; font-family: 'Poppins', sans-serif;">
                                            Lorem
                                            Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                            unknown printer took a
                                            galley of type and scrambled it to make a type specimen book. It has survived
                                            not only five centuries, but also the
                                            leap into electronic typesetting, remaining essentially unchanged. It was
                                            popularised in the 1960s with the release
                                            of Letraset sheets containing Lorem Ipsum passages, and more recently with
                                            desktop
                                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                        </p>
                                    </h4>
                                </div>
                            </div>

                        </div>







                    </div>


                </div>

                <br>

                <center>
                    <h3 class="des" style="letter-spacing: 4px;">
                        Cari Kami Di Media Sosial
                    </h3>
                </center>




                <div class="row">
                    <div class="col-lg-4">

                        <div class="containerr2">
                            <div class="card2">
                                <div class="imgBx2">
                                    <img src="facebook4.png">
                                </div>
                                <div class="contentBx2">
                                    <h2>Selangor TV</h2>

                                    <br>

                                    <a href="https://facebook.com/mediaselangor">GO</a>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="col-lg-4">

                        <div class="containerr3">
                            <div class="card3">
                                <div class="imgBx3">
                                    <img src="tiktok.png">
                                </div>
                                <div class="contentBx3">
                                    <h2>Selangor TV</h2>

                                    <br>

                                    <a href="https://www.tiktok.com/@selangortv?lang=en">GO</a>
                                </div>
                            </div>
                        </div>

                    </div>





                    <div class="col-lg-4">

                        <div class="containerr">
                            <div class="card">
                                <div class="imgBx">
                                    <img src="youtube.png">
                                </div>
                                <div class="contentBx">
                                    <h2>Selangor TV</h2>

                                    <br>

                                    <a href="https://www.youtube.com/c/TVSelangorMY">GO</a>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>


            </div>

        </div>

    </div>

@endsection

@prepend('script')

    <script type="text/javascript">
        WowzaPlayer.create('playerElement',
        {
            "license": "PLAY1-8r4CZ-9hYuD-TXbPf-x8n7w-eXZwn",
            "title": "Selangor Live!",
            "description": "Selangor Live Streaming",
            "sourceURL": "http://172.16.2.39:1935/live/myStream/playlist.m3u8",
            "autoPlay": false,
            "volume": "75",
            "mute": false,
            "loop": false,
            "audioOnly": false,
            "uiShowQuickRewind": true,
            "uiQuickRewindSeconds": "30"
        });
    </script>



@endprepend
