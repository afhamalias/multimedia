@extends('layouts.master')

@prepend('style')

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap');
        @import url("https://fonts.googleapis.com/css?family=Libre+Franklin:400,700&subset=latin");
        @import url(https://fonts.googleapis.com/css?family=Roboto);

        * {
            -webkit-font-smoothing: antialiased;
            box-sizing: border-box;
        }

        h1,
        h2,
        h3,
        h4 {
            padding: 0;
            margin: 0.1rem 0;
            border-left: 4px solid black;
            padding-left: 8px;
        }

        .material-tabs {
            display: block;
            float: left;
            padding: 16px;
            padding-top: 0;
            width: 100%;
            max-width: 480px;
            left: calc(50% - 480px/2);
            position: relative;
            margin: 96px auto;
            background: #fff;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23) !important;
            border-radius: 2px;
        }

        @media all and (max-width: 480px) {
            .material-tabs {
                max-width: 100%;
                left: 0;
            }
        }

        .visible {
            position: relative;
            opacity: 1;
            width: 100%;
            height: auto;
            float: left;
            transition: opacity 0.35s ease;
            z-index: 3;
        }

        .hidden {
            position: absolute;
            opacity: 0;
            z-index: 0;
            transition: opacity 0s ease;
        }

        .hidden img {
            display: none;
        }

        [class*=tabbed-section-] {
            float: left;
            color: #000;
        }

        [class*=tabbed-section-] img {
            display: block;
            width: 80%;
            margin: auto 10%;
        }

        .tabbed-section__selector {
            position: relative;
            height: 32px;
            top: -31.2px;
            left: -16px;
            padding: 0;
            margin: 0;
            width: 100%;
            float: left;
        }

        .tabbed-section__selector [class*=-tab-] {
            float: left;
            display: block;
            height: 32px;
            line-height: 32px;
            width: 100px;
            text-align: center;
            background: #fff;
            font-weight: bold;
            text-decoration: none;
            color: black;
            font-size: 14px;
        }

        .tabbed-section__selector [class*=-tab-].active {
            color: white;
        }

        .tabbed-section__selector a:first-child {
            border-top-left-radius: 2px;
        }

        .tabbed-section__selector a:last-of-type {
            border-top-right-radius: 2px;
        }

        .tabbed-section__highlighter {
            position: absolute;
            z-index: 10;
            bottom: 0;
            height: 2px;
            background: black;
            max-width: 100px;
            width: 100%;
            transform: translateX(0);
            display: block;
            left: 0;
            transition: transform 0.23s ease;
        }

        .tabbed-section__selector-tab-3.active~.tabbed-section__highlighter {
            transform: translateX(200px);
        }

        .tabbed-section__selector-tab-2.active~.tabbed-section__highlighter {
            transform: translateX(100px);
        }

        .tabbed-section__selector-tab-1.active~.tabbed-section__highlighter {
            transform: translateX(0);
        }

        .divider {
            background: rgba(0, 0, 0, 0.1);
            position: relative;
            display: block;
            float: left;
            width: 100%;
            height: 1px;
            margin: 8px 0;
            padding: 0;
            overflow: hidden;
        }

    </style>

@endprepend



@section('content')


    <div class="material-tabs">
        <div class="tabbed-section__selector">
            <a class="tabbed-section__selector-tab-1 active" href="#">Tajuk</a>
            <a class="tabbed-section__selector-tab-2" href="#">Penerangan</a>
            <a class="tabbed-section__selector-tab-3" href="#">Tarikh</a>

            <span class="tabbed-section__highlighter"></span>
        </div>


        <div class="tabbed-section-1 visible">
            <p>{{ $music->title }}</p>
        </div>
        <div class="tabbed-section-2 hidden">
            <p>{{ $music->description }}</p>
        </div>
        <div class="tabbed-section-3 hidden">
            <p>{{ $music->date }}</p>
        </div>
    </div>


    <button onclick="history.back()" class="btn btn-danger" >Back</button>

@endsection






@prepend('script')

    <script>
        var $ = function(selector) {
            return document.querySelectorAll(selector);
        };


        // Define tabs, write down them classes
        var tabs = [
            '.tabbed-section__selector-tab-1',
            '.tabbed-section__selector-tab-2',
            '.tabbed-section__selector-tab-3'
        ];

        // Create the toggle function
        var toggleTab = function(element) {
            var parent = element.parentNode;

            // Do things on click
            $(element)[0].addEventListener('click', function() {
                // Remove the active class on all tabs.
                // climbing up the DOM tree with `parentNode` and target
                // the children ( the tabs ) with childNodes
                this.parentNode.childNodes[1].classList.remove('active');
                this.parentNode.childNodes[3].classList.remove('active');
                this.parentNode.childNodes[5].classList.remove('active');

                // Then, give `this` (the clicked tab), the active class
                this.classList.add('active');

                // Check if the clicked tab contains the class of the 1 or 2
                if (this.classList.contains('tabbed-section__selector-tab-1')) {
                    // and change the classes, show the first content panel
                    $('.tabbed-section-1')[0].classList.remove('hidden');
                    $('.tabbed-section-1')[0].classList.add('visible');

                    // Hide the second
                    $('.tabbed-section-2')[0].classList.remove('visible');
                    $('.tabbed-section-2')[0].classList.add('hidden');
                    $('.tabbed-section-3')[0].classList.remove('visible');
                    $('.tabbed-section-3')[0].classList.add('hidden');
                }

                if (this.classList.contains('tabbed-section__selector-tab-2')) {
                    // and change the classes, show the second content panel
                    $('.tabbed-section-2')[0].classList.remove('hidden');
                    $('.tabbed-section-2')[0].classList.add('visible');
                    // Hide the first
                    $('.tabbed-section-1')[0].classList.remove('visible');
                    $('.tabbed-section-1')[0].classList.add('hidden');
                    $('.tabbed-section-3')[0].classList.remove('visible');
                    $('.tabbed-section-3')[0].classList.add('hidden');
                }

                if (this.classList.contains('tabbed-section__selector-tab-3')) {
                    // and change the classes, show the second content panel
                    $('.tabbed-section-3')[0].classList.remove('hidden');
                    $('.tabbed-section-3')[0].classList.add('visible');
                    // Hide the first
                    $('.tabbed-section-1')[0].classList.remove('visible');
                    $('.tabbed-section-1')[0].classList.add('hidden');
                    $('.tabbed-section-2')[0].classList.remove('visible');
                    $('.tabbed-section-2')[0].classList.add('hidden');
                }
            });
        };

        // Then finally, iterates through all tabs, to activate the
        // tabs system.
        for (var i = tabs.length - 1; i >= 0; i--) {
            toggleTab(tabs[i])
        };
    </script>
@endprepend
