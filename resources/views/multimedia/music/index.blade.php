@extends('layouts.master')

@prepend('style')

    <style>
        /* html.open,
                    body.open {
                        height: 100%;
                        padding-top: 40%;
                        overflow: hidden; */
        }


        p {
            text-align: center;
            margin: 20px 0 60px;
        }




        h1 {
            text-align: center;
            font-weight: 300;
        }

        table {
            display: block;
        }

        tr,
        td,
        tbody,
        tfoot {
            display: block;
        }

        thead {
            display: none;
            text-align: center;
        }

        tr {
            padding-bottom: 10px;
            text-align: center;
        }

        td {
            padding: 10px 10px 0;
            text-align: center;
        }


        td:before {
            content: attr(data-title);
            color: #7a91aa;
            text-transform: uppercase;
            font-size: 1.4rem;
            padding-right: 10px;
            display: block;
        }

        table {
            width: 80%;
        }

        th {
            text-align: center;
            font-weight: 700;
        }

        thead th {
            background-color: white;
            color: black;
            font-size: 20px;
            border: 4px solid black;
        }

        tfoot th {

            padding: 10px;
            text-align: center;
            color: #b8c4d2;
        }

        .button {
            background-color: #4b908f;
            color: white;
            padding: 12px 30px;
            cursor: pointer;
            font-size: 20px;
            line-height: 1;
            display: inline-block;
            font-size: 1.2rem;
            text-decoration: none;
            border-radius: 5px;
            color: #fff;
            padding: 8px;
        }

        .select {
            padding-bottom: 20px;
            border-bottom: 1px solid #28333f;
        }

        .select:before {
            display: none;
        }

        /* .detail {
                        background-color: maroon;
                        width: 100%;
                        height: 100%;
                        padding: 40px 0;
                        position: fixed;
                        top: 0;
                        left: 0;
                        overflow: auto;
                        -moz-transform: translateX(-100%);
                        -ms-transform: translateX(-100%);
                        -webkit-transform: translateX(-100%);
                        transform: translateX(-100%);
                        -moz-transition: -moz-transform 0.3s ease-out;
                        -o-transition: -o-transform 0.3s ease-out;
                        -webkit-transition: -webkit-transform 0.3s ease-out;
                        transition: transform 0.3s ease-out;
                    }

                    .detail.open {
                        -moz-transform: translateX(0);
                        -ms-transform: translateX(0);
                        -webkit-transform: translateX(0);
                        transform: translateX(0);
                    }

                    .detail-container {
                        margin: 0 auto;
                        padding: 40px;
                        max-width: 1000px;
                    }

                    dl {
                        margin: 0;
                        padding: 0;
                        color: white;
                    }

                    dt {
                        font-size: 2.2rem;
                        font-weight: 300;
                        color: white;
                    }

                    dd {
                        margin: 0 0 40px 0;
                        font-size: 1.8rem;
                        padding-bottom: 5px;
                        border-bottom: 1px solid black;
                        box-shadow: 0 1px 0 #c52c51;
                        color: white;
                    } */

        /* .close {
                        background: white;
                        padding: 18px;
                        color: #fff;
                        border: 4px solid rgba(255, 255, 255, 0.4);
                        border-radius: 8px;
                        line-height: 1;
                        font-size: 50px;
                        position: fixed;
                        right: 40px;
                        top: 20px;
                        -moz-transition: border 0.3s linear;
                        -o-transition: border 0.3s linear;
                        -webkit-transition: border 0.3s linear;
                        transition: border 0.3s linear;
                    }

                    .close:hover,
                    .close:focus {
                        background-color: white;
                        border: 4px solid #a82545;
                    } */



        @media (min-width: 460px) {
            td {
                text-align: center;
            }

            td:before {
                display: inline-block;
                text-align: right;
                width: 140px;
            }

            .select {
                padding-left: 160px;
            }
        }

        @media (min-width: 720px) {
            table {
                display: table;
            }

            tr {
                display: table-row;
            }

            td,
            th {
                display: table-cell;
            }

            tbody {
                display: table-row-group;
            }

            thead {
                display: table-header-group;
            }

            tfoot {
                display: table-footer-group;
            }

            td {
                border: 1px solid #28333f;
            }

            td:before {
                display: none;
            }

            td,
            th {
                padding: 10px;
            }

            tr:nth-child(2n+2) td {
                background-color: #242e39;
            }

            tfoot th {
                display: table-cell;
            }

            .select {
                padding: 10px;
            }

            .color {
                color: black;
            }


            .bg {
                background-color: white;

            }

            .container-audio {
                width: 66%;
                height: auto;
                padding: 20px;
                border-radius: 5px;
                background-color: #eee;
                color: black;
                margin: 20px auto;
            }

            audio {
                width: 100%;
            }

            audio:nth-child(2),
            audio:nth-child(4),
            audio:nth-child(6) {
                margin: 0;
            }

            .container-audio .colum1 {
                width: 23px;
                height: 5em;
                border-radius: 5px;
                margin: 0 10px 0 0;
                display: inline-block;
                position: relative;
            }

            .container-audio .colum1:last-child {
                margin: 0;
            }



            .btnD {
                background-color: #4b908f;
                color: white;
                padding: 12px 30px;
                cursor: pointer;
                font-size: 20px;
                line-height: 1;
                display: inline-block;
                font-size: 1.2rem;
                text-decoration: none;
                border-radius: 5px;
                color: #fff;
                padding: 8px;
            }
        }

    </style>

@endprepend




@section('content')


    {{-- <h1> Audio <i class="fas fa-headphones-alt" style="color: maroon"></i> Gallery </h1> --}}

    <br>

    <main>


        <center>
            <table>
                <thead>
                    <tr>
                        <th>
                            Tajuk
                        </th>
                        <th>
                            Audio
                        </th>
                        <th>
                            Muat Turun
                        </th>
                    </tr>
                </thead>


                @foreach ($music as $m)



                    <tbody>



                        {{-- content 1 --}}
                        <tr>
                            <td data-title='Title' class="color bg">
                                {{ $m->title }}
                            </td>

                            <td data-title='player' class="bg container-audio">
                                <audio controls>
                                    <source src="{{ url('/data_file/' . $m->audio) }}" type="audio/mpeg">
                                </audio>
                            </td>

                            <td class='select bg'>
                                {{-- <a class="btnD" href="{{ route('music.details', $m->id) }}">
                                    <i class="fas fa-info"></i> Details
                                </a> --}}


                                <a class="btnD" href="{{ url('/data_file/' . $m->audio) }}" download>
                                    <i class="fa fa-download"></i> Muat Turun
                                </a>
                            </td>
                        </tr>
                    </tbody>


                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
                        aria-labelledby="myLargeModalLabel" aria-hidden="true">

                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">

                                    <h5 class="modal-title" id="exampleModalLabel">{{ $m->title }}</h5>

                                </div>

                                <div class="modal-body">
                                    {{ $m->description }}
                                </div>

                                <div class="modal-footer">
                                    {{ $m->date }}
                                </div>

                            </div>
                        </div>
                    </div>




                    {{-- <div class='detail color'>
                        <div class='detail-container'>
                            <dl>
                                <dt>
                                    Title
                                </dt>
                                <dd>
                                    {{ $m->title }}
                                </dd>


                                <dt>
                                    Description
                                </dt>
                                <dd>
                                    {{ $m->description }}
                                </dd>



                                <dt>
                                    Upload Date
                                </dt>
                                <dd>
                                    {{ $m->date }}
                                </dd>



                            </dl>
                        </div>

                        <div class='detail-nav'>
                            <button class='close'>
                                Close
                            </button>
                        </div>
                    </div> --}}



                @endforeach


            </table>
        </center>



    </main>
@endsection



@prepend('script')

    <script>
        $('.button, .close').on('click', function(e) {
            e.preventDefault();
            $('.detail, html, body').toggleClass('open');
        });


        // function goBack() {
        //     window.history.back();
        // }
    </script>


@endprepend
