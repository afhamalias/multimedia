@extends('layouts.master')

@prepend('style')

    <style>
        /* .text-box {
                                margin-left: 44vw;
                                margin-top: 42vh;
                            }

                            .btn:link,
                            .btn:visited {
                                text-transform: uppercase;
                                text-decoration: none;
                                padding: 15px 40px;
                                display: inline-block;
                                border-radius: 100px;
                                transition: all .2s;
                                position: absolute;
                            }

                            .btn:hover {
                                transform: translateY(-3px);
                                box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
                            }

                            .btn:active {
                                transform: translateY(-1px);
                                box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
                            }

                            .btn-white {
                                background-color: red;
                                color: rgb(255, 255, 255);
                            }

                            .btn::after {
                                content: "";
                                display: inline-block;
                                height: 100%;
                                width: 100%;
                                border-radius: 100px;
                                position: absolute;
                                top: 0;
                                left: 0;
                                z-index: -1;
                                transition: all .4s;
                            }

                            .btn-white::after {
                                background-color: #fff;
                            }

                            .btn:hover::after {
                                transform: scaleX(1.4) scaleY(1.6);
                                opacity: 0;
                            }

                            .btn-animated {
                                animation: moveInBottom 5s ease-out;
                                animation-fill-mode: backwards;
                            }

                            @keyframes moveInBottom {
                                0% {
                                    opacity: 0;
                                    transform: translateY(30px);
                                }

                                100% {
                                    opacity: 1;
                                    transform: translateY(0px);
                                }
                            } */

        .title {
            font-size: 20px;
            color: grey;
        }



        video {
            width: 100%;
            height: 100%;
        }

        .moreVid {
            width: 100%;
            height: 180px;

        }

        h1 {
            font-size: 30px;
            text-align: center;
            font-family: 'Zen Kurenaido', sans-serif;
        }

        img.icon {
            width: 60px;
            height: 70px;
            padding-bottom: 10px;
        }

        p.des {
            font-size: 25px;
        }

        .bg {
            background-color: maroon;
            color: white;
        }

        /* .fixedbutton {
            position: absolute;
            bottom: 80px;
        } */

    </style>

@endprepend



@section('content')
    <div class="container">


        <div class="row">


            {{-- Video live --}}
            <div class="col-lg-8">
                <div class="col">
                    <center>
                        <video src="{{ url('/data_file/' . $video->video) }}" controls> </video>
                    </center>

                    <br>



                    <p class="des"> {{ $video->title }} </p>



                    <br>


                    {{-- Description Below / Live Description --}}
                    <div class="card card-primary bg">
                        <div class="card-body">
                            {{-- <h5 class="card-title"> Author : </h5> --}}

                            <br>

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <p style="font-size: 15px">
                                                {{ $video->description }}

                                            </p>


                                        </h4>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>
            </div>




            {{-- Surve Video on Rigth --}}


            <div class="col-lg-4">

                {{-- @foreach ($random as $r) --}}


                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">

                            <div class="card-header">
                                <h5 class="m-0">Video Terkini</h5>
                            </div>


                            <div class="card-body">
                                <iframe class="moreVid" src="{{ url('/data_file/' . $video) }}" frameborder="0"
                                    allowfullscreen ng-show="showvideo">
                                </iframe>

                                <p class="card-text">Tajuk untuk video sebelumnya</p>
                                {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12">

                        <div class="card">

                            <div class="card-body">
                                <iframe class="moreVid" src="https://www.youtube.com/embed/cVyhH3t49fs"
                                    frameborder="0" allowfullscreen ng-show="showvideo">
                                </iframe>

                                <p class="card-text">Tajuk untuk video sebelumnya</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                        </div>
                    </div>


                </div>


                {{-- @endforeach --}}

            </div>

        </div>






    </div>


    {{-- <div class="fixedbutton">
        <button type="button" class="btn btn-outline-primary" onclick="goBack()">BACK</button>
    </div> --}}





@endsection

@prepend('script')

    {{-- <script>
        function goBack() {
            window.history.back();
        }
    </script> --}}
@endprepend
