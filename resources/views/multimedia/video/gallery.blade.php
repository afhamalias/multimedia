@extends('layouts.master')

@prepend('style')



    <style>
        @import url('https://fonts.googleapis.com/css2?family=Kalam&display=swap');


        body {
            margin: 0;
        }

        .video-section {
            display: grid;
            grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
            gap: 3rem 1rem;
            padding: 3rem 0;
            margin: 0 1.5rem;
            border-top: 4px solid #CCC;
        }

        .video-section:first-child {
            border-top: none;
        }

        .video-container {
            display: flex;
            flex-direction: column;
        }

        .thumbnail {
            position: relative;
            display: flex;
        }

        .thumbnail::before {
            content: attr(data-duration);
            position: absolute;
            background-color: rgba(0, 0, 0, .85);
            color: white;
            right: 5px;
            bottom: 5px;
            padding: .1em .3em;
            border-radius: .3em;
            font-size: .9rem;
        }

        .thumbnail-image {
            width: 100%;
            height: 100%;
            min-width: 250px;
            min-height: 150px;
            background-color: #AAA;
        }

        .video-bottom-section {
            display: flex;
            align-items: flex-start;
            margin-top: 1rem;
        }

        .channel-icon {
            margin-right: .75rem;
            border-radius: 50%;
            width: 36px;
            height: 36px;
            background-color: #AAA;
        }

        .video-details {
            display: flex;
            flex-direction: column;
        }

        .video-title {
            font-size: 1.1rem;
            font-weight: bold;
            margin-bottom: .5rem;
            text-decoration: none;
            color: black;
        }

        .video-channel-name {
            margin-bottom: .1rem;
            text-decoration: none;
            transition: color 150ms;
            font-family: 'Kalam', cursive;
        }

        .video-channel-name:hover {
            color: grey;
        }

        .video-channel-name,
        .video-metadata {
            color: black;
        }

        .video-section-title {
            grid-column: 1 / -1;
            margin: -.5rem 0;
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 0 .5rem;
        }

        .video-section-title-close {
            border: none;
            background: none;
            padding: 0;
            font-size: 2rem;
            color: #555;
            cursor: pointer;
        }

        .videos {
            background-color: #F9F9F9;
        }

        h3 {
            text-align: center;
        }

        /* .fixedbutton {
                position: absolute;
                bottom: 80px;
            } */

    </style>

@endprepend




@section('content')

    {{-- <h3> VIDEO GALLERY </h3> --}}


    <div class="container">

        <div class="video">


            <section class="video-section">

                @foreach ($video as $v)

                    <article class="video-container">
                        <a href="{{ route('video.player', $v->id) }}" class="thumbnail">


                            <img class="thumbnail-image" src="{{ url('/data_file/' . $v->thumbnail) }}" alt="">

                        </a>

                        <div class="video-bottom-section">

                            <div class="video-details">
                                <a href="{{ route('video.player', $v->id) }}"
                                    class="video-title">{{ $v->title }}</a>
                                <a href="{{ route('video.player', $v->id) }}" class="video-channel-name">
                                    {{ $v->date }}</a>
                                <div class="video-metadata">
                                </div>
                            </div>
                        </div>
                    </article>


                @endforeach



            </section>

        </div>
    </div>


    {{-- <div class="fixedbutton">
        <button type="button" class="btn btn-outline-primary" onclick="goBack()">BACK</button>
    </div> --}}




@endsection



@prepend('script')

    {{-- <script>
     function goBack() {
            window.history.back();
        }
</script> --}}
@endprepend
