@extends('layouts.master')

@prepend('style')

    <style>
        /* *,
                        *::before,
                        *::after {
                            margin: 0;
                            padding: 0;
                            box-sizing: border-box;
                        }

                        body {
                                width: 100%;
                                height: 100vh;
                                background: #f1f2f6;
                                display: flex;
                                justify-content: center;
                                align-items: center;
                            } */


        .title {
            font-family: 'Helvetica Neue', sans-serif;
            font-size: 18px;
        }

        .folder {
            transition: all 0.2s ease-in;
        }

        .folder_back {
            position: relative;
            width: 100px;
            height: 80px;
            background: #4786ff;
            border-radius: 0 5px 5px 5px;
        }

        .folder_back::after {
            content: "";
            width: 30px;
            height: 10px;
            background: #4786ff;
            position: absolute;
            bottom: 98%;
            left: 0;
            border-radius: 5px 5px 0 0;

        }

        .folder_back .paper {
            width: 70%;
            height: 80%;
            background: #e6e6e6;
            position: absolute;
            bottom: 10%;
            left: 50%;
            transform: translate(-50%, 10%);
            border-radius: 5px;
            transition: all 0.3 ease-in-out;
        }

        .folder_back .paper.paper:nth-child(2) {
            background: #f2f2f2;
            width: 80%;
            height: 70%;
        }

        .folder_back .paper.paper:nth-child(3) {
            background: white;
            width: 90%;
            height: 60%;
        }

        .folder_back .folder_front {
            position: absolute;
            width: 100%;
            height: 100%;
            background: #70a1ff;
            background-image: url('live.png');
            imgae border-radius: 5px;
            transform-origin: bottom;
            transition: all 0.3s ease-in-out;
        }

        .folder:hover {
            transform: translateY(-8px);
        }

        .folder:hover .paper {
            transform: translateY(-50%, 0%);
        }

        .folder:hover .folder_front {
            transform: skew(15deg) scaleY(0.6)
        }

        .folder:hover .right {
            transform: skew(-15deg) scaleY(0.6)
        }

        /* .fixedbutton {
                position: absolute;
                bottom: 80px;
            } */

    </style>
@endprepend




@section('content')

    <div class="container">


        <div class="row">



            @foreach ($folder as $f)
                <div class="col-lg-3">

                    <div class="card">
                        <center>


                            <div class="car-header">
                                <span class="title"> {{ $f->title }} </span>
                            </div>

                            <div class="card-body">

                                <a href="{{ route('picture.View', $f->id) }}" class="folder">

                                    <div class="folder_back">
                                        <div class="paper"></div>
                                        <div class="paper"></div>
                                        <div class="paper"></div>
                                        <div class="folder_front"></div>
                                        <div class="folder_front right"></div>
                                    </div>

                                </a>
                            </div><span> Upload Date : <b> {{ $f->date }} </b></span>

                        </center>
                    </div>

                </div>
            @endforeach
        </div>


        <br>

        {{-- <div class="fixedbutton">
        <button type="button" class="btn btn-outline-primary" onclick="goBack()">BACK</button>
    </div> --}}

{{-- --------------------------------------------------------------------------------------------------------------------------------------- --}}

    </div>
@endsection

@prepend('script')

    {{-- <script>
        function goBack() {
            window.history.back();
        }
    </script> --}}

@endprepend
