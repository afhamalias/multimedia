@extends('layouts.master')

@prepend('style')

    <style>
        *,
        *:before,
        *:after {
            box-sizing: border-box;
        }

        /* * {
                -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                transform-style: preserve-3d;
            } */

        *:focus {
            outline: none !important;
        }

        body,
        html {
            height: 100%;
        }

        body {
            height: 100%;
            font-family: faune, fantasy;
        }

        .photo-grid {
            position: relative;
            font-style: italic;
        }

        .photo-grid ul {
            display: grid;
            grid-template-columns: 1fr;
            list-style: none;
        }

        .photo-grid ul li {
            width: 100%;
            overflow: hidden;
        }

        .photo-grid figure,
        .photo-grid picture,
        .photo-grid img {
            display: block;
            max-width: 100%;
        }

        .photo-grid figure {
            position: relative;
        }

        .photo-grid figcaption {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .photo-grid fieldset {
            /* display: flex; */
            align-items: flex-end;
            justify-content: flex-end;
            width: 100%;
            height: 100%;
            padding: 10px;
        }

        .photo-grid .photo-close {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 1;
            width: 100%;
            height: 100%;
        }

        .photo-grid label,
        .photo-grid .photo-link {
            cursor: pointer;
        }

        .photo-grid .photo-link {
            will-change: opacity;
            display: block;
            position: relative;
            z-index: 1;
            padding: 3px;
        }

        .photo-grid .icon {
            display: block;
            width: 25px;
            height: 25px;
        }

        .photo-grid .icon group,
        .photo-grid .icon path,
        .photo-grid .icon polyline,
        .photo-grid .icon polygon,
        .photo-grid .icon circle,
        .photo-grid .icon ellipse,
        .photo-grid .icon rect {
            fill: none;
            fill-rule: evenodd;
            stroke: white;
            stroke-width: 7px;
            stroke-linecap: round;
            stroke-linejoin: round;
        }

        .photo-grid input {
            display: none;
        }

        .photo-grid input~label.photo-link {
            display: none;
        }

        .photo-grid input:checked~dl {
            transform: none;
            opacity: 1;
            z-index: 2;
        }

        .photo-grid input:checked~.photo-close {
            z-index: unset;
        }

        .photo-grid input:checked~label.photo-link {
            opacity: 0;
        }

        .photo-grid dl {
            transform: translateY(25%);
            opacity: 0;
            z-index: 0;
            transition: all 0.3s ease-out;
            will-change: opacity, transform;
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            padding: 12px;
            font-size: 24px;
            color: white;
            background-color: rgba(17, 23, 26, 0.9);
        }

        .photo-grid dl label,
        .photo-grid dl .photo-link {
            position: absolute;
            right: 0;
            z-index: 1;
            padding: 6px;
        }

        .photo-grid dl label {
            top: 0;
        }

        .photo-grid dl .photo-link {
            bottom: 0;
        }

        .photo-grid dl div {
            position: relative;
            z-index: 0;
        }

        .photo-grid dl dt,
        .photo-grid dl dd {
            display: inline;
        }

        .photo-grid dl dt {
            opacity: 0.7;
            font-weight: bold;
        }

        .photo-grid dl dt:after {
            content: ": ";
        }

        @supports (-webkit-backdrop-filter: blur(10px)) {
            .photo-grid dl {
                background-color: rgba(17, 23, 26, 0.4);
                -webkit-backdrop-filter: blur(5px);
                backdrop-filter: blur(5px);
            }
        }

        @media only screen and (min-width: 600px) {
            .photo-grid input~label.photo-link {
                opacity: 0;
            }

            .photo-grid li:hover input~label.photo-link {
                opacity: 1;
            }

            .photo-grid li:hover input:checked~label.photo-link {
                opacity: 0;
            }

            .photo-grid input~label.photo-link {
                display: block;
            }
        }

        @media only screen and (min-width: 800px) {
            .photo-grid ul {
                grid-template-columns: 1fr 1fr;
            }
        }

        @media only screen and (min-width: 2000px) {
            .photo-grid ul {
                grid-template-columns: 1fr 1fr 1fr;
            }
        }

        @media only screen and (min-width: 3000px) {
            .photo-grid ul {
                grid-template-columns: 1fr 1fr 1fr 1fr;
            }
        }

    </style>

@endprepend




@section('content')

    <main ontouchstart>
        <section class='photo-grid'>
            <div class="row">


                <ul class='grid-isotope'>
                    @foreach ($gambars->where('folder_id', $folder->id) as $g )

                    <div class="col-lg-12">

                        <li class='photo-grid-item'>

                            <figure>

                                <picture>
                                    <source media='(max-width: 3000px)'>

                                    <img alt='' src="{{ url('/data_file/' . $g->picture) }}">
                                </picture>

                                <figcaption>


                                    <fieldset>

                                        <input id='{{$g->id}}' type='checkbox'>
                                        <label class='photo-close' for='{{$g->id}}'></label>
                                        <label class='photo-link' for='{{$g->id}}'>
                                            <svg class='icon' viewBox='0 0 100 100'>
                                                <circle cx='49' cy='49' r='36'></circle>
                                                <path d='M45,69 L55,69 M45,39 L50,39 L50,69 M49.5,29 L50,29'></path>
                                            </svg>
                                        </label>
                                        <dl>

                                            <label for='{{$g->id}}'>
                                                <svg class='icon' viewBox='0 0 100 100'>
                                                    <polyline points='14 32 50 68 86 32'></polyline>
                                                </svg>
                                            </label>


                                            <div>
                                                <dt>Tajuk</dt>
                                                <dd>{{$g->title}}</dd>
                                            </div>
                                            <div>
                                                <dt>Penerangan</dt>
                                                <dd>{{$g->description}}</dd>
                                            </div>
                                            <div>
                                                <dt>Tarikh</dt>
                                                <dd>{{$g->date}}</dd>
                                            </div>



                                            <a class='photo-link'
                                                href="{{ url('/data_file/' . $g->picture) }}"
                                                tabindex='-1' target='_blank'>
                                                <svg class='icon' viewBox='0 0 100 100'>
                                                    <path
                                                        d='M82,38 L82,78.9930191 C82,80.6537288 80.663269,82 78.9989882,82 L21.0010118,82 C19.3435988,82 18,80.663269 18,78.9989882 L18,21.0010118 C18,19.3435988 19.3408574,18 21.0069809,18 L62,18 M88.9559283,10.8111066 L57.9878833,42.132705 M69.2453268,10.8994949 L89.0443166,10.8994949 L89.0443166,30.6984848'>
                                                    </path>
                                                </svg>
                                            </a>
                                        </dl>
                                    </fieldset>


                                </figcaption>



                            </figure>

                        </li>

                    </div>
                    @endforeach

                </ul>
            </div>
        </section>
    </main>

    <!-- /.row -->
@endsection


@prepend('script')
@endprepend
