<?php

namespace App\Http\Controllers;

use App\Models\folder;
use Illuminate\Http\Request;
use Session;

class folderController extends Controller
{
    public function folder()
    {
        $folder = folder::get();
        return view('admin.pictureFolder',['folder' => $folder]);
    }

    public function store(Request $request){
		$this->validate($request, [
            'title' => 'required',
            'date' => 'required',
		]);

        folder::create([
			'title' => $request->title,
            'date' => $request->date,

		]);


        Session::flash('success','Folder has been created !!');

		return redirect()->back();
	}

    public function deleteFolder($id){
        // hapus file
		$folder = folder::where('id',$id)->first();


		// hapus data
		folder::where('id',$id)->delete();

        Session::flash('delete','Folder has been succesfully delete !!');

		return redirect()->back();
    }



}
