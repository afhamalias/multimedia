<?php

namespace App\Http\Controllers;

use App\Models\folder;
use App\Models\Gambar;
use Illuminate\Http\Request;

class PictureController extends Controller
{
    public function index()
    {
        $folder = folder::all();
        return view('multimedia.picture.index',  compact('folder'));
    }

    public function pictureView($id)
    {
        $folder=folder::find($id);
        $gambars=Gambar::all();
        return view('multimedia.picture.page2', compact('folder', 'gambars'));



    }

    // public function pictureEdit()
    // {
    //     return view('admin.pictureE');


    // }
}
