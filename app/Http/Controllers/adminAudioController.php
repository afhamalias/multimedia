<?php

namespace App\Http\Controllers;

use App\Models\Music;
use Illuminate\Http\Request;
use File;
use Session;

class adminAudioController extends Controller
{

    public function uploadAudio(){
        $audio = Music::get();
		return view('admin/audioE',['audio' => $audio]);
	}

	public function proses_uploadAudio(Request $request){
        // to store data in database
		$this->validate($request, [
			'audio' => 'required',
			'title' => 'required',
            'description' => 'required',
            'start' => date("Y-m-d", strtotime($request->datepicker)),
            'end' => date("Y-m-d", strtotime($request->datepicker1)),

		]);




        // Upload data to temp folder
        $file = $request->file('audio');

		$nama_file = time()."_".$file->getClientOriginalName();

      	// isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$file->move($tujuan_upload,$nama_file);

		Music::create([
			'audio' => $nama_file,
			'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date,

		]);

        Session::flash('success','Audio successfully added !!');

        return redirect()->back();
	}

    public function deleteAudio($id){
        // hapus file
		$audio = Music::where('id',$id)->first();
		File::delete('data_file/'.$audio->file);

		// hapus data
		Music::where('id',$id)->delete();

        Session::flash('delete','Audio has been succesfully delete !!');

		return redirect()->back();
    }



}
