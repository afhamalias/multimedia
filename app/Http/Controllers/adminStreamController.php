<?php

namespace App\Http\Controllers;

use App\Models\Streaming;
use Illuminate\Http\Request;
use File;
use Session;

class adminVideoController extends Controller
{
    public function editStream(){
        $stream = Streaming::get();
		return view('admin/stremE',['stream' => $stream]);
	}

	public function updateData(Request $request){
        // to store data in database
		$this->validate($request, [
			'title' => 'required',
            'description',
            'start' => date("Y-m-d", strtotime($request->datepicker)),
            'end' => date("Y-m-d", strtotime($request->datepicker1)),

		]);

		Streaming::create([
			'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date,
		]);


        Session::flash('success','Data has been update successfully !!');

        return redirect()->back();
	}

}
