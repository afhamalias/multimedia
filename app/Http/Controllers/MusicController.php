<?php

namespace App\Http\Controllers;

use App\Models\Music;
use Illuminate\Http\Request;

class MusicController extends Controller
{
    public function index()
    {
        $music = Music::all();
        return view('multimedia/music/index', compact("music"));
    }


    public function details($id)
    {
        $music = Music::find($id);
        return view('multimedia.music.details', compact("music"));
    }


    public function audioEdit()
    {
        return view('admin.audioE');
    }
}
