<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index()
    {
        $video=Video::all();
        return view('multimedia.video.gallery', compact("video"));


    }


    public function videoPlayer($id)
    {
        $video=Video::find($id);
        // $random=Video::all();
        return view('multimedia.video.player',compact("video"));


    }

    public function videoEdit()
    {
        return view('admin.videoE');
    }


}
