<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;
use File;
use Session;

class adminVideoController extends Controller
{
    public function uploadVideo(){
        $video = Video::get();
		return view('admin/videoE',['video' => $video]);
	}

	public function proses_uploadvideo(Request $request){
        // to store data in database
		$this->validate($request, [
			'video' => 'required',
            'thumbnail' => 'required|file|image|mimes:jpeg,png,jpg',
			'title' => 'required',
            'description' => 'required',
            'start' => date("Y-m-d", strtotime($request->datepicker)),
            'end' => date("Y-m-d", strtotime($request->datepicker1)),

		]);




        // Upload data to temp folder
        $file = $request->file('video');
        $thumbnail = $request->file('thumbnail');

		$nama_file = time()."_".$file->getClientOriginalName();
        $gambarThumbnail = time()."_".$thumbnail->getClientOriginalName();

      	// isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$file->move($tujuan_upload,$nama_file);
        $thumbnail->move($tujuan_upload,$gambarThumbnail);

		Video::create([
			'video' => $nama_file,
            'thumbnail' => $gambarThumbnail,
			'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date,

		]);

        Session::flash('success','Video successfully added !!');

        return redirect()->back();
	}

    public function deletevideo($id){
        // hapus file
		$video = Video::where('id',$id)->first();
		File::delete('data_file/'.$video->file);

		// hapus data
		Video::where('id',$id)->delete();

        Session::flash('delete','Video has been succesfully delete !!');

		return redirect()->back();
    }
}
