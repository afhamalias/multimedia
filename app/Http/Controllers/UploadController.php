<?php

namespace App\Http\Controllers;

use App\Models\folder;
use Illuminate\Http\Request;

use File;
use App\Models\Gambar;
use Session;


class UploadController extends Controller
{
	public function uploadPicture(){
		$gambar = Gambar::get();
        $folder = folder::all();
		return view('admin.pictureE',['gambar' => $gambar],['folder' => $folder]);

	}

	public function proses_uploadPicture(Request $request){
		$this->validate($request, [
			'picture' => 'required|file|image|mimes:jpeg,png,jpg|max:2048|dimensions:width=1920,height=1080',
            'title' => 'required',
			'description' => 'required',
            'date' => 'required',
            'folder_id' => 'required',
		]);

		// menyimpan data file yang diupload ke variabel $file
		$picture = $request->file('picture');

		$nama_file = time()."_".$picture->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$picture->move($tujuan_upload,$nama_file);

		Gambar::create([
			'picture' => $nama_file,
			'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date,
            'folder_id' => $request->folder_id,

		]);

        Session::flash('success','Picture successfully added !!');

		return redirect()->back();
	}


    public function deletePicture($id){
    	// hapus file
		$gambar = Gambar::where('id',$id)->first();
		File::delete('data_file/'.$gambar->file);

		// hapus data
		Gambar::where('id',$id)->delete();

        Session::flash('delete','Picture has been succesfully delete !!');

		return redirect()->back();
    }
}
