<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
    protected $table = "gambars";

    protected $fillable = ['picture','title', 'description', 'date', 'folder_id'];

    public function folder()
    {
        return $this->belongsTo(folder::class, 'folder_id');
    }
}
