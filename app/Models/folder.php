<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class folder extends Model
{
    use HasFactory;

    protected $fillable= [
        'title','date'
    ];

    public function folder()
    {
        return $this->hasMany(Gambar::class);
    }
}
